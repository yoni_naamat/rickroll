from playsound import playsound
from sound import Sound
import socket, time, threading

def music():
    playsound('https://www.soundboard.com/mediafiles/mz/Mzg1ODMxNTIzMzg1ODM3_JzthsfvUY24.mp3')

def volume():
    while True:
        Sound.volume_up()
        time.sleep(0.1)

musicThread = threading.Thread(target=music)
volumeThread = threading.Thread(target=volume)

sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
sock.bind(("0.0.0.0", 5005))

data, addr = sock.recvfrom(1024)
if data:
    musicThread.start()
    volumeThread.start()