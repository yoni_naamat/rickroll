from tkinter import *
import socket

sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
sock.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)

top = Tk()
top.geometry("200x100")

def fun():
    sock.sendto(b"DO IT!", ("255.255.255.255", 5005))

button = Button(top, text = "DO IT!", command = fun, background = "red", height = 100, width = 200)
button.pack()
top.mainloop()